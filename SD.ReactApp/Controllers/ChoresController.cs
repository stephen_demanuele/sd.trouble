﻿using System;
using System.Linq;
using SD.ReactApp.Models;
using SD.ReactApp.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace SD.ReactApp.Controllers
{
	[Route("api/chores")]
	[ApiController]
	public class ChoresController : ControllerBase
	{
		private readonly IChoreRepository _choreRepository;
		private readonly ILogger<ChoresController> _logger;

		public ChoresController(IChoreRepository choreRepository, ILogger<ChoresController> logger)
		{
			_choreRepository = choreRepository;
			_logger = logger;
		}

		[HttpGet]
		public ActionResult<IEnumerable<string>> Get()
		{
			return Ok(_choreRepository.Get(x => true));
		}

		[HttpPost]
		public IActionResult Post([FromBody] Chore chore)
		{
			_choreRepository.Add(chore);

			return Created($"api/chores/{chore.Id}", chore);
		}

		[HttpGet("{id:guid}")]
		public IActionResult Get(Guid id)
		{
			var chores = _choreRepository.Get(x => x.Id == id);
			if (chores == null || !chores.Any())
				return NotFound();

			return Ok(chores.First());
		}

		[HttpDelete("{id:guid}")]
		public IActionResult Delete(Guid id)
		{
			if (_choreRepository.Remove(id))
			{
				return Ok();
			}

			return NotFound();
		}
	}
}