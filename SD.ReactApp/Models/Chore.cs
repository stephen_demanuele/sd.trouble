﻿using System;

namespace SD.ReactApp.Models
{
	public class Chore
	{
		public Guid Id { get; set; }

		public DateTime? Date { get; set; }

		public string DateFormatted => Date.HasValue ? Date.Value.ToString("dd-MMM-yy HH:mm") : "No date";

		public string Label { get; set; }

		public string Description { get; set; }
	}
}
