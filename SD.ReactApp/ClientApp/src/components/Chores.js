﻿import React, { Component } from 'react';
import Chore from './Chore';

export class Chores extends Component {
    static displayName = Chores.name;

    static renderChores(chores) {
        return (
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Label</th>
                        <th>Description</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        chores.map(chore => <Chore key={chore.id} subject={chore} actionOnDelete={this.refresh}/>
                    )}
                </tbody>
            </table>
        );
    }

    constructor(props) {
        super(props);
        this.state = { chores: [], loading: true, currentCount: 0 };
        this.act = this.act.bind(this);
        this.refresh = this.refresh.bind(this);

        this.refresh();
    }

    refresh() {
        fetch('api/chores')
            .then(response => response.json())
            .then(data => {
                this.setState({ chores: data, loading: false });
            });
    }

    act() {
        this.setState({
            chores: this.state.chores,
            loading: this.state.loading
        });
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Chores.renderChores(this.state.chores);

        return (
            <div>
                <h1>Chores</h1>
                <p>Showing all chores</p>
                {contents}
            </div>
        );
    }
}