﻿import React, { Component } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

export default class Chore extends Component {
    static displayName = Chore.name;

    constructor(props) {
        super(props);

        this.state = 
            {
                chore: props.subject,
                onDelete: props.actionOnDelete
            };
    }

    submit = () => {
        confirmAlert({
            title: 'Confirm to submit',
            message: 'Delete chore?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.handleDeleteClick(this.state.chore.id)
                },
                {
                    label: 'Foggeddabawd it',
                    onClick: () => alert('Ok')
                }
            ]
        });
    };

    handleDeleteClick = choreId => {
        const requestOptions = { method: "DELETE" };

        fetch('api/chores/' + choreId, requestOptions)
            .then((response) => {
                if (response.status !== 200) {
                    alert(response.statusText);
                }
                else {
                    alert('calling onDelete');
                    this.state.onDelete;
                    alert('called onDelete');
                }
            });
    }

    render() {
        return (
            <tr key={this.state.chore.id}>
                <td>
                    <button onClick={this.submit}>Delete</button>
                </td>

                <td>{this.state.chore.label}</td>
                <td>{this.state.chore.description}</td>
                <td>{this.state.chore.dateFormatted}</td>
            </tr>
        );
    }
}