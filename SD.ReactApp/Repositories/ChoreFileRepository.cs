﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using SD.ReactApp.Models;
using System.Collections.Generic;
using SD.ReactApp.Contracts;
using Microsoft.Extensions.Logging;
using System.Text.Json.Serialization;

namespace SD.Trouble.WebApp.Repositories
{
	internal class ChoreFileRepository : IChoreRepository
	{
		private readonly ILogger<ChoreFileRepository> _logger;

		public ChoreFileRepository(ILogger<ChoreFileRepository> logger)
		{
			_logger = logger;
			Load();
		}

		private List<Chore> Items { get; set; } = null;

		public void Add(Chore chore)
		{
			chore.Id = Guid.NewGuid();
			if (!chore.Date.HasValue)
				chore.Date = DateTime.Now;

			Items.Add(chore);
			Save();
		}

		public bool Remove(Guid Id)
		{
			var chore = Items.Find(x => x.Id.Equals(Id));
			if (chore == null)
				return false;

			Items.Remove(chore);
			Save();

			return true;
		}

		public IReadOnlyList<Chore> Get(Expression<Func<Chore, bool>> expr)
		{
			try
			{
				return Items.Where(expr.Compile()).ToList().AsReadOnly();
			}
			catch (Exception ex)
			{
				_logger.LogCritical(ex, "Error searching chores");

				throw;
			}
		}

		public void Load()
		{
			try
			{
				var filepath = GetRepositoryFilePath();
				if (!File.Exists(filepath))
				{
					var fileinfo = new FileInfo(filepath);
					if (!fileinfo.Directory.Exists)
					{
						fileinfo.Directory.Create();
					}

					using (var filestream = File.Create(filepath))
					{
						filestream.Close();
						Items = new List<Chore>();

						return;
					}
				}

				var all = File.ReadAllText(filepath);
				Items = JsonSerializer.Parse<List<Chore>>(all);
			}
			catch (Exception ex)
			{
				_logger.LogCritical(ex, "Error loading chores");

				throw;
			}
		}

		private void Save()
		{
			try
			{
				var options = new JsonSerializerOptions
				{
					WriteIndented = true
				};

				var serializedAll = JsonSerializer.ToString<List<Chore>>(Items, options);

				var filepath = GetRepositoryFilePath();
				File.WriteAllText(filepath, serializedAll);
			}
			catch (Exception ex)
			{
				_logger.LogCritical(ex, "Error saving chores");

				throw;
			}
		}

		private string GetRepositoryFilePath()
		{
			return Path.Combine(Environment.CurrentDirectory, "Data", "chores.json");
		}
	}
}
