﻿using System;
using SD.ReactApp.Models;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace SD.ReactApp.Contracts
{
	public interface IChoreRepository
	{
		void Load();

		void Add(Chore chore);

		bool Remove(Guid Id);

		IReadOnlyList<Chore> Get(Expression<Func<Chore, bool>> expr);
	}
}